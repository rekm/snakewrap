.. Snakewrap documentation master file, created by
   sphinx-quickstart on Wed Sep 30 12:30:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Snakewrap's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   install
   your_first_app
   wrapper_deep_dive
   snakewrap 
   modules

Purpose of the library
======================

The Goal of this library is to streamline the creation of user friendly
wrapping scripts for calling snakemake pipelines.
Snakemake has lots of peculiarities and users might just want to use your
pipeline without browsing snakemakes documentation first.


It also contains a filename parser scheme that can parse paired-end 
illumina reads from filenames and pair them up correctly.
It is designed to run on huge folders containing sequencing runs. 

Using Snakewrap
===============

Snakewrap is a library and needs to be installed first.

All you need to get started is running 

.. code-block:: console
    
    pip install snakewrap


Afterwards you should be able to run this code

.. code-block:: python
    
    from snakewrap.wrapper import DefaultApp 
    
    app = DefaultApp("yourapp")
    app.set_snakefile("my_snake_file.smk")
    app.run()



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
