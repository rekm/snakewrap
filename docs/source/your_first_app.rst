=======================
Your First SnakeWrapper
=======================


Given that you installed the library, you can now write your first snakemake
wrapper.

First runnable app from scratch 
###############################

Setup a project folder
----------------------

Lets create a snakemake app from scratch. 
First we create a project directory

.. code-block:: bash
    
    mkdir -p "my_snake_app/"{rules,envs,scripts,tests} # create directories
    touch "my_snake_app/my_snake_app."{py,smk} # create files we need
    chmod +x my_snake_app/my_snake_app.py
	tree my_snake_app # show our new project directory contents

This should yield: 

::

   	my_snake_app/
	├── envs
	├── my_snake_app.py
	├── my_snake_app.smk
	├── rules
	└── scripts

Writing the first piece of code
-------------------------------

Now we need to populate "my_snake_app.py" with code  

.. code-block:: python 
    :linenos:
    :caption: my_snake_app.py



    #!/usr/bin/env python 
    from snakewrap.wrapper import SnakeApp
    import argparse
    import os

    class App(SnakeApp):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs) #initialize parent class
     
        def args_to_config_dict(self):
            config = vars(self.args) # just turn namespace to dict and return it
            return config 

        def get_argparser(self):
            parser = argparse.ArgumentParser()
            parser.add_argument("--input")
            parser.add_argument("--config")
            parser.add_argument("-o", "--outdir", type=os.path.realpath, default=".")
            return parser # return a argument parser
        
        def init_setup(self):
            if self.args.outdir is not None:
                self.set_workdir(self.args.outdir) # set working directory

You can see that there are a couple necessary steps before you have a 
viable app. 

You absolutely need to overwrite the args_to_config_dir() method 
and also need to overwrite get_argparser with a method that returns an argument parser.

init_setup() is used to modify the internal state of the class with values provided by the arguments and is optional, but required to get the parser to do anything more than just calling snakemake with default parameters. You should probably also set your snakemake file path relative to this file. 

Getting the app to run
----------------------

Now lets create our app by adding these lines to my_snake_app.py

.. code-block:: python 
    :lineno-start: 24 
    :caption: my_snake_app.py

    # Actually create an instance of our app
    app = App("dummyapp")
    # Set Snakefile path
    app.set_snakefile(
        os.path.join(os.path.dirname(os.path.realpath(".")),
                     "my_snake_app.smk")
    app.add_log_file("my_snake_app.log")
    app.run()

You should either use set snakefile in the class code and allow it to use
a command line option, or use it like it is shown here, to hardcode the snakefile location relative to the python file. 

Yay, you have a snakeapp!
-------------------------

We can now run the app in the following manner 

.. code-block:: bash

    my_snake_app/my_snake_app.py -h 
    

This app does not yet do much, but it should already call snakemake for you
