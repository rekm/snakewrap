SnakeWrap API
=============


Module contents
###############

.. automodule:: snakewrap
   :members:
   :undoc-members:
   :show-inheritance:



Submodules
##########

snakewrap.wrapper module
------------------------


.. automodule:: snakewrap.wrapper
   :members:
   :undoc-members:
   :show-inheritance:

snakewrap.sample_manager module
-------------------------------

.. automodule:: snakewrap.sample_manager
   :members:
   :undoc-members:
   :show-inheritance:

snakewrap.sample_manager.illumina_sample_manager module
-------------------------------------------------------

.. automodule:: snakewrap.sample_manager.illumina_sample_manager
   :members:
   :undoc-members:
   :show-inheritance:
   
snakewrap.utils module
----------------------

.. automodule:: snakewrap.utils
   :members:
   :undoc-members:
   :show-inheritance:


