Installation
============

The minimum required python version is 3.5  

There are several ways of installing this library


Installation with pip
---------------------

    * regular install

        .. code-block:: bash
            
            pip3 install snakewrap 

    * install without root

        .. code-block:: bash
         
            pip3 install --user snakewrap


    * virtual environment install 
        
        First install virtualenv, if it is not installed already
        We use the python3 module call "python3 -m ..." to ensure that we
        don't python2 pip. 

        .. code-block:: bash
        
            python3 -m pip install --user virtualenv

        Same as before but we now use venv to create a virtual environment 
        called snakewrap_env in the current working directory 

        .. code-block:: bash
        
            python3 -m venv snakewrap_env

        We now activate the environment by issuing the following command

        .. code-block:: bash
        
            source snakewrap_env/bin/activate
        
        We are now in the activated environment.
        You can confirm this on linux systems by running ``which python``.
        It should point into your snakewrap_env
        
        After you have confirmed this, we no longer need to worry about
        using the wrong python or pip. 
        You can simply run the next command and install the library into
        the virtual environment.

        .. code-block:: bash

            pip install snakewrap 
        
        to exit the virtual environment simply run 

        .. code-block:: bash

            deactivate

        Whenever you want to play around with the library simply source
        the activate sctipt again 

Installation with conda
-----------------------
    

    .. code-block:: bash
        
       conda install -c bioconda snakewrap
