#!/bin/bash

set -eEuo pipefail

SKIPCONDAINSTALL=0
while getopts s flag; do
    case "$flag" in 
        s) SKIPCONDAINSTALL=1
           ;;
    esac  
done
shift $(( $OPTIND -1 )) 

ENVNAME="wrapper_env"
SOURCE="$( realpath "${BASH_SOURCE[0]}" )"
SOURCE_DIR="$( dirname "$SOURCE" )"
ENV_SETUP="${SOURCE_DIR}/env_setup"
OUTDIR=${1-.}
ENVPATH="${OUTDIR}/${ENVNAME}"
APATH="${ENVPATH}/etc/conda/activate.d"
DPATH="${ENVPATH}/etc/conda/deactivate.d"
OPT_PATH="${ENVPATH}/opt/ncov_minipipe"
SNAKE_DIR="$( realpath "${SOURCE_DIR}/.." )"

echo $OUTDIR

stop_setup(){
    unset ENVNAME SOURCE SOURCE_DIR ENV_SETUP OUTDIR ENVPATH APATH DPATH
    unset OPT_PATH SNAKE_DIR SKIPCONDAINSTALL
    exit 0
}

update_conda_or_install(){
    conda install \
        -c defaults -c conda-forge  -c r -c bioconda \
        "python>=3.6.0" \
        snakemake \
        pip \
        sphinx \
        pyyaml \
        sphinxcontrib-napoleon \
        pandas \
        strictyaml \
        -ymp "${ENVPATH}"
}

[ $SKIPCONDAINSTALL -eq 1 ] || update_conda_or_install

mkdir -p "${APATH}"
mkdir -p "${DPATH}"

cp "${ENV_SETUP}/a.welcome.sh" "${ENV_SETUP}/a.env_vars.sh" "${APATH}"
cp "${ENV_SETUP}/d.env_vars.sh" "${DPATH}"

# Customization

mkdir -p "$OPT_PATH"
set +eEu
source activate "${ENVPATH}/"
set +eEu
cd "${SOURCE_DIR}/../../"
python -m pip install --no-deps --ignore-installed .
conda deactivate

stop_setup

