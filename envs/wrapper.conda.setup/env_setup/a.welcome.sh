#!/bin/bash



printf "%s\n" \
    "Loaded environment: wrapper-env" \
    "Location: $CONDA_PREFIX"\
    " "\
    "$(conda list |& grep -E 'snakemak|snakewrap|^yaml|^perl\s|^python|strictyaml|pandas|csv')" \
    " "\
    "This environment is used to control environment tools for the pipeline" \
    " "\
    "simply run:" \
    "       swrap-quicksetup --help"\
    ""\
    "Use the library:"\
    "  python"\
    "  import snakewrap as sw"\
    "  sw.App(\"foobar\").run()"
