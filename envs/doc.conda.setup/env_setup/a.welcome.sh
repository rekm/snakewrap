#!/bin/bash



printf "%s\n" \
    "Loaded environment: documentation for wrapper" \
    "Location: $CONDA_PREFIX"\
    " "\
    "$(conda list |& grep -E 'snakemake|^yaml|^perl\s|^python|strictyaml|pandas|csv')" \
    " "\
    "This environment is used to control environment tools for the pipeline" \
    " "\
    "simply run:" \
    "       cd docs"\
    "       make html" 
